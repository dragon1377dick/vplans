﻿using Newtonsoft.Json;
using System.IO;

namespace vplans.Configuration
{
    class Config
    {
        private const string CONFIG_FILE = "./config.json";

        public string VplansUsername { get; set; } = string.Empty;
        public string VplansPassword { get; set; } = string.Empty;
        public string VplansClassRegex { get; set; } = @"^(([56789]|10)[a-g])|ef|q[12]$";
        public string VplansSpecificClassRegex { get; set; } = @"^\d{1,2}";
        public string VplansYearRegex { get; set; } = @"^\d{1,2}$";
        public string VplansUrl { get; set; } = string.Empty;
        public string BotToken { get; set; } = string.Empty;
        public long OperatorId { get; set; } = 0;
        [JsonIgnore]
        public bool OperatorIdIsSet { get => OperatorId != 0; }
        public bool LogDebugEvents { get; set; } = false;
        public bool ShowConsole { get; set; } = true;

#pragma warning disable IDE0079 // Unnötige Unterdrückung entfernen
#pragma warning disable IDE1006 // Benennungsstile
#pragma warning disable IDE0051 // Nicht verwendete private Member entfernen
        public string __VplansUsername { get; } = @"(Nur wenn nötig) Der Benutzername, um an den Onlinevertretungsplan zu kommen. Dieser wird auch später im Bot selber vom Benutzer abgefragt.";
        public string __VplansPassword { get; } = @"(Nur wenn nötig) Das Passwort zum Benutzernamen, um an den Onlinevertretungsplan zu kommen. Dieser wird auch später im Bot selber vom Benutzer abgefragt.";
        public string __VplansClassRegex { get; } = @"Nur valide Klassen stimmen mit dem RegEx überein. (regex101.com). Standard: ^(([56789]|10)[a-g])|ef|q[12]$";
        public string __VplansSpecificClassRegex { get; } = @"Nur Klassen (z.B. 5c, 9d, 10f), welche von einem 'Jg 07' oder '05, 06' betroffen sein können (= nicht Sek II) stimmen mit diesem RegEx überein. Wenn dies geändert wird, dann wird wahrscheinlich auch ein anderer Teil (im Programmcode) angepasst werden müssen! Standard: ^\d{1,2}";
        public string __VplansYearRegex { get; } = @"Nur Jahrgänge stimmen mit diesem RegEx überein (z.B. 5, 7, 6, 8, 9, 10) Standard: ^\d{1,2}$";
        public string __VplansUrl { get; } = "Die URL des online Vertretungsplans von UNTIS. Das {0:D3} wird durch eine dreistellige Zahl mit führenden nullen ersetzt. Beispiel: http://gesamtschule-xyz.de/vplans/subst_{0:D3}.htm";
        public string __BotToken { get; } = "Der Telegram Bot Token";
        public string __OperatorId { get; } = "(Optional)(Ganzzahl) Die TelegramId der Person, welche den Bot betreibt. Diese bekommt Statusmeldungen über den Bot, z.B. wenn dieser startet oder über den Tag Fehler aufgetreten sind.";
        public string __LogDebugEvents { get; } = "(Optional)(Boolean) Wenn true, dann werden mehr Informationen über die aktuellen Vorgänge protokolliert.";
        public string __ShowConsole { get; } = "(Optional)(Boolean) Wenn true, dann bleibt die Konsole sichtbar. Andernfalls verschwindet diese nach dem ersten update.";
#pragma warning restore IDE1006 // Benennungsstile
#pragma warning restore IDE0051 // Nicht verwendete private Member entfernen
#pragma warning restore IDE0079 // Unnötige Unterdrückung entfernen

        private Config() { }

        private static readonly object LOCK = new object();
        private static Config cfg;

        public static Config GetInstance()
        {
            lock (LOCK)
            {
                if (cfg == null)
                {
                    LoadConfig();
                }

                return cfg;
            }
        }

        private static void LoadConfig()
        {
            if (File.Exists(CONFIG_FILE))
            {
                string json = File.ReadAllText(CONFIG_FILE);
                cfg = JsonConvert.DeserializeObject<Config>(json);
            }
            else
            {
                cfg = new Config();
            }

            File.WriteAllText(CONFIG_FILE, JsonConvert.SerializeObject(cfg, Formatting.Indented));

            string errors = string.Empty;

            if (string.IsNullOrWhiteSpace(cfg.BotToken))
                errors += "BotToken nicht definiert.\r\n";

            if (string.IsNullOrWhiteSpace(cfg.VplansUrl))
                errors += "VplansUrl ist nicht definiert.\r\n";

            if (string.IsNullOrWhiteSpace(cfg.VplansUsername))
                errors += "VplansUsername ist nicht definiert.\r\n";

            if (string.IsNullOrWhiteSpace(cfg.VplansPassword))
                errors += "VplansPassword ist nicht definiert.\r\n";

            if (string.IsNullOrWhiteSpace(cfg.VplansClassRegex))
                errors += "VplansClassRegex nicht definiert\r\n";

            if (string.IsNullOrWhiteSpace(cfg.VplansSpecificClassRegex))
                errors += "VplansSpecificClassRegex nicht definiert.\r\n";

            if (string.IsNullOrWhiteSpace(cfg.VplansYearRegex))
                errors += "VplansYearRegex nicht definiert.\r\n";

            if (errors.Length > 0)
            {
                throw new ConfigurationException(errors + "\r\nDie Kommentare in der config.json können als Hilfestellung verwendet werden.");
            }
        }
    }
}
