﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vplans.Vertretungsplan
{
    class VClass : IEquatable<VClass>
    {
        private static readonly IEqualityComparer<string[]> STR_ARR_COMPR = new ArrayEqualityComparer<string>();

        public DateTime Date { get; private set; }
        public string ClassName { get; private set; } = "Class missing";

        //DEKO
        private int[] sizing = null;
        private bool[] columnEmpty = null;

        #region [Fach vs. (Fach)]
        //There are usually two columns; Fach and (Fach). Usually these contain the same info. So maybe skip them?
        private int fach1 = -1;
        private int fach2 = -1;
        private int art = -1;
        private bool omitFach2 = false;//<- (Fach) usually is noted first
        #endregion

        private readonly List<string[]> entries = new List<string[]>();

        public VClass(string className, DateTime date, string[] headers, string[] colums)
        {
            ClassName = className;
            Date = date;

            sizing = new int[headers.Length];
            columnEmpty = new bool[headers.Length];

            PreprocessColumns(headers);
            for (int i = 0; i < headers.Length; i++) //<- Find (Fach) and Fach
            {
                switch (headers[i])
                {
                    case "(Fach)": fach1 = i; break;
                    case "Fach": fach2 = i; break;
                    case "Art": art = i; break;
                }

                columnEmpty[i] = true; //<- Initialize all to true
            }
            omitFach2 = fach1 != -1 && fach2 != -1 && art != -1;
            entries.Add(headers);

            Add(colums);
        }

        public VClass() { }

        public void Add(string[] cols)
        {
            PreprocessColumns(cols);
            entries.Add(cols);
        }

        public void Add(VClass vClass)
        {
            if (sizing == null)
            {
                ClassName = vClass.ClassName;
                Date = vClass.Date;

                omitFach2 = vClass.omitFach2;
                fach1 = vClass.fach1;
                fach2 = vClass.fach2;
                art = vClass.art;

                int columnCount = vClass.sizing.Length;
                sizing = new int[columnCount];
                columnEmpty = new bool[columnCount];

                for (int i = 0; i < columnCount; i++)
                {
                    sizing[i] = vClass.sizing[i];
                    columnEmpty[i] = vClass.columnEmpty[i];
                }

                entries.AddRange(vClass.entries);
            }
            else
            {
                omitFach2 &= vClass.omitFach2 && fach1 == vClass.fach1 && fach2 == vClass.fach2;
                entries.AddRange(vClass.entries.GetRange(1, vClass.entries.Count - 1)); //<- Skip the headers

                for (int i = 0; i < sizing.Length; i++)
                {
                    if (sizing[i] < vClass.sizing[i])
                    {
                        sizing[i] = vClass.sizing[i];
                        columnEmpty[i] &= sizing[i] == 0;
                    }
                }
            }
        }

        private void PreprocessColumns(string[] cols)
        {
            string[] replaceables = { "&nbsp;", "",
                "eigenverantwortliches Arbeiten", "evA",
                "Statt-Vertretung", "S-Vertretung",
                "(Klassen(n))", "Klasse",
                "---", "",
                "Aufgaben", "Aufg." };

            for (int i = 0; i < sizing.Length; i++)
            {
                for (int j = 0; j < replaceables.Length; j += 2)
                {
                    cols[i] = cols[i].Replace(replaceables[j], replaceables[j + 1]);
                }

                cols[i] = cols[i].Trim();
                int contentLen = cols[i].Length;

                columnEmpty[i] &= contentLen == 0 || cols[i] == "+" || cols[i] == "---";
                if (sizing[i] < contentLen)
                {
                    sizing[i] = contentLen;
                }
            }

            omitFach2 = omitFach2 && (cols[fach1] == cols[fach2] || cols[art] == "Entfall" || cols[art] == "evA");
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder().AppendLine($"*{Date:dd.MM.yyyy}:*");

            if (entries.Count > 0)
            {
                builder.Append("`");
                foreach (string[] columns in entries)
                {
                    for (int i = 0; i < sizing.Length; i++)
                    {
                        if (!(omitFach2 && fach2 == i) && !columnEmpty[i])
                        {
                            builder.AppendFormat($"{{0,-{sizing[i] + 1}}}", columns[i]);
                        }
                    }

                    builder.AppendLine();
                }
                builder.AppendLine("`");
            }

            return builder.ToString();
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as VClass);
        }

        public bool Equals(VClass other)
        {
            bool equal = true;
            if (!ReferenceEquals(this, other))
            {
                equal = other is VClass && this.Date == other.Date;

                int thisEntriesCount = this.entries.Count;
                equal = equal && thisEntriesCount == other.entries.Count;

                for (int i = 0; equal && i < thisEntriesCount; i++)
                {
                    equal = this.entries.Contains(other.entries[i], STR_ARR_COMPR);
                }
            }

            return equal;
        }

        public override int GetHashCode()
        {
            int hashCode = -1542377900;
            hashCode *= -1521134295 + this.Date.GetHashCode();

            foreach (string[] entry in this.entries)
            {
                foreach (string cell in entry)
                {
                    hashCode *= -1521134295 + cell.GetHashCode();
                }
            }

            return hashCode;
        }

        public static bool operator ==(VClass a, VClass b) => ReferenceEquals(a, b) || (a is VClass && b is VClass && a.Equals(b));

        public static bool operator !=(VClass a, VClass b) => !(a == b);
    }
}
