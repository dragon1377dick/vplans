﻿using System.Reflection;

namespace vplans.Logic
{
    class Texts
    {
        #region INFO
        public static readonly string INFO = "*vplans Bot v" + Assembly.GetExecutingAssembly().GetName().Version.ToString(3) + "*" + @"
Programmierung und Umsetzung:
 © trienow 2016 - 2022

Verwendete Bibliotheken:
- Dapper v2.0
- HtmlAgilityPack v1.11
- Newtonsoft.Json v13.0
- Serilog v2.10
- System.Data.SQLite.Core v1.0
- System.Net.Requests v4.3
- Telegram.Bot v17.7";
        #endregion

        public const string HILFE = @"Das sind die Befehle, die du benutzen kannst:
/vertretung - Zeigt dir alle Vertretungen deiner Klasse / deines Jahrgangs an
/vertretung <Klassenname> - Zeigt den Vertretungsplan von <Klassenname> an.

/hilfe - Ruft diese Hilfe auf
/erinnerung - Schaltet Erinnerungen bei Vertretungsplanupdates ein/aus
/abbrechen - Bricht einen Vorgang ab
/zuruecksetzen - Setzt alle Einstellungen an diesem Bot zurück (Passiert nach 14 Monaten Inaktivität automatisch)
/klasse - Wechselt deine aktive Klasse / Jahrgang
/info - Zeigt Informationen über diesen Bot an";

        public const string ZURÜCKSETZEN = "Ok. Nach dieser Nachricht habe ich dich vergessen.";

        public const string ABBRECHEN_NICHTS = "Aktuell gibt es nichts abzubrechen.";
        public const string ABBRECHEN_OK = "Vorgang abgebrochen.";

        public const string ERINNERUNG = "Erinnerungen: ";

        public const string START00 = "Hi! Ich schaue kurz nach, ob ich dich kenne...";
        public static string Start01(string className) => $"Bist du nicht in der *{className}*?";

        public const string START02 = "Ich kenne dich noch nicht.";
        public const string START03 = "Gebe bitte den **Benutzernamen** für den Onlinevertretungsplan ein:";
        public const string START04 = "Gebe bitte das **Passwort** für den Onlinevertretungsplan ein:";
        public const string START05 = START04;
        public const string START06 = "Leider war das Passwort oder der Benutzername inkorrekt.";
        public const string START07 = "Ok.";

        public const string START08 = "In welche Klasse gehst du?";
        public const string START09 = "Hmmm. Bist du dir sicher, dass deine Eingabe richtig war? Laut meinem Regelwerk existiert deine angegebene Klasse nicht. Versuche es nochmal.";
        public const string START10 = "**ERFOLG!** Ich habe dich nun gespeichert.";
        public const string START11 = START07;
        public const string START12 = @"Hier noch ein paar Tipps:
- Schreibe oder Klicke auf /vertretung um den aktuellen Plan an zu zeigen
- Bei einem Vertretungsplanupdate für deine Klasse, bekommst du eine Benachrichtigung
- Mit /hilfe werden dir alle Optionen angezeigt

Viel Spaß!";

        public const string KLASSE_FALSCH = "Die angeforderte Klasse kenne ich nicht. Bitte sende mir den Befehl mit dem Format:\r\n`/klasse <Klassenname>`\r\n\r\nZwei Beispiele:\r\n`/klasse 8b` oder \r\n`/klasse q1`";

        public const string SPAM = "Ich glaube du hast mich mit Cookie Clicker oder so verwechselt :(\r\nSodass dies nicht nochmal vorkommt, schweige ich die nächste Minute.";
        public const string GOODBYE = "Hi! Da ich seit langem nichts mehr von dir gehört habe, gehe ich davon aus, dass du auch nichts mehr von mir hören willst und werde dich aus meiner Datenbank löschen.\r\nBis dann und machs' gut! :)\r\n\r\nEine erneute Einrichtung mit /start ist möglich.";
        public const string UPGRADE_INVALID_CLASS = @"Hi!
Ich habe den Bot upgedatet, bzw. komplett neugeschrieben. Dieser sollte jetzt auch Schüler in der EF/Q1/Q2 unterstützen.
Falls du den Bot weiterhin verwenden willst, richte dich bitte mit /start neu ein. Ansonsten wünsche ich dir noch viel Erfolg!";

        public const string KLASSE_WECHSEL = "Klasse zu {0} gewechselt.";
        public const string WEBPLAN_ERROR = "Die letzten Versuche, den Vertretungsplan zu ermitteln sind alle fehlgeschlagen.\r\nAus diesem Grund kann aktuell kein gültiger Plan angezeigt werden.";
    }
}
