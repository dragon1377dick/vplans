﻿using Telegram.Bot.Types.Enums;

namespace vplans.Telegrammer
{
    public class MessageAnimation
    {
        public MessageAnimation(string content)
        {
            this.Content = content;
        }

        public int Delay { get; set; } = 35;
        public string Content { get; set; }

        public ChatAction DelayAction { get; set; } = ChatAction.Typing;

        public MessageType MessageType { get; set; } = MessageType.Text;
        public ParseMode? ParseMode { get; set; } = Telegram.Bot.Types.Enums.ParseMode.Markdown;

        public override string ToString() => this.Content;

        public static implicit operator string(MessageAnimation messageAnimation) => messageAnimation.Content;
    }
}
