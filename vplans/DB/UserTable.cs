﻿using System;
using vplans.Logic;

namespace vplans.DB
{
    public class UserTable
    {
        internal const string CREATE = "CREATE TABLE IF NOT EXISTS Users ( `ID` INTEGER, `Class` TEXT, `ReminderI` INTEGER, `LastSeenI` INTEGER, `AntiSpamI` INTEGER, `Activity` INTEGER, `ActivityStep` INTEGER, PRIMARY KEY(`ID`));";
        internal const string INSERT = "INSERT INTO Users VALUES(@ID, @Class, @ReminderI, @LastSeenI, @AntiSpamI, @Activity, @ActivityStep);";
        internal const string UPDATE = "UPDATE Users SET Class = @Class, ReminderI = @ReminderI, LastSeenI = @LastSeenI, AntiSpamI = @AntiSpamI, Activity = @Activity, ActivityStep = @ActivityStep WHERE ID = @ID";
        internal const string UPDATE_ANTISPAM = "UPDATE Users SET AntiSpamI = @AntiSpamI WHERE ID = @ID";
        internal const string SELECT = "SELECT * FROM Users WHERE ID = @ID";
        internal const string SELECT_INVALID = "SELECT * FROM Users WHERE Class = 'INVALID'";
        internal const string SELECT_OBSOLETE = "SELECT * FROM Users WHERE LastSeenI < @MinLastSeen";
        internal const string DELETE = "DELETE FROM Users WHERE ID = @ID";

        internal const string COUNT = "SELECT COUNT(*) FROM Users";

        /// <summary>
        /// Telegram UserID
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Associated Class
        /// </summary>
        public string Class { get; set; } = null;

        public int ReminderI { get => Reminder ? 1 : 0; set => Reminder = value == 1; }

        /// <summary>
        /// When <see langword="true"/>, then the user gets a reminder
        /// </summary>
        public bool Reminder { get; set; } = true;

        public long LastSeenI { get => LastSeen.Ticks; set => LastSeen = new DateTime(value); }

        /// <summary>
        /// When the <see cref="UserTable"/> last contacted the bot
        /// </summary>
        public DateTime LastSeen { get; set; } = new DateTime();

        public long AntiSpamI { get => AntiSpam.Ticks; set => AntiSpam = new DateTime(value); }
        public DateTime AntiSpam { get; set; } = DateTime.MinValue;

        /// <summary>
        /// Defines the Users current activity, e.g. Setting up something
        /// </summary>
        public ActivityContext Activity { get; set; } = ActivityContext.IDLE;

        public int ActivityStep { get; set; } = 0;
    }
}
