﻿namespace vplans.DB
{
    class MetaTable
    {
        internal const string CREATE = "CREATE TABLE IF NOT EXISTS Meta (`ID` INTEGER PRIMARY KEY AUTOINCREMENT, `Version` INTEGER);";
        internal const string SELECT = "SELECT * FROM Meta";
        internal const string INSERT = "INSERT INTO Meta VALUES(NULL, @Version)";
        internal const string UPDATE = "UPDATE Meta SET Version = @Version WHERE ID = @ID";
        internal const int CURRENT_VERSION = 0;

        public int ID { get; set; } = 0;
        public int Version { get; set; } = CURRENT_VERSION;
    }
}
