
# VPLANS Telegram Bot

Ein Telegram Bot, um die Handhabung von Online-Vertretungsplänen zu vereinfachen.

## Funktionen
- Abrufen des Vertretungsplans
- Filter für spezifische Klassen
- Benachrichtigung bei Änderungen
- Einfache Authentifizierung von Zugriffen
- Erweiterte Informationen für Betreiber

## Kompatibilität
Aktuell funktioniert das Programm nur für Onlinevertretungsplänen der Software "Untis 2017". Ob folgende Versionen auch funktionieren, ist unklar. Gerne kann man mir über die Funktionalität Rückmeldung geben.

# Nutzung
## Systemvoraussetzungen
- Windows
- Internetzugriff
- .NET Framework 4.8

Bei genügend Interesse, portiere ich es evtl. zu .NET Core, um es auch auf Linux betreiben zu können.

## Schnellstart
Siehe im Wiki: https://gitlab.com/trienow/vplans/-/wikis/Eigenen-Bot-Betreiben

# Wartung
Sollte es Probleme geben, kann gerne ein Ticket aufgemacht werden. Pull-Requests sind auch willkommen. Da ich selber von diesem Bot keinen Nutzen habe, werde ich eine zeitnahe Bearbeitung nicht garantieren.

# Hinweise
Dieses Projekt ist als eigenes Projekt entstanden. Die Entwickler und dieses Projekt steht in keiner Verbindung zu Firmen, welche Stunden- und Vertretungsplansoftware entwickeln.

