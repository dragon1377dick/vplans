﻿using System;
using System.IO;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using vplans.Vertretungsplan;

namespace Tests
{
    [TestClass]
    public class VPlanerTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext ctx)
        {
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
        }

        [TestMethod]
        public void Test00()
        {
            const string BASEDIR = @"TestData\VPlaner\00\";
            VPlaner vp = new VPlaner();

            vp.UpdatePlan(BASEDIR);
            vp.GetVPlan("5a").Should().Be(File.ReadAllText(BASEDIR + "5a.md"));
            vp.GetVPlan("10a").Should().Be(File.ReadAllText(BASEDIR + "10a.md"));
            vp.GetVPlan("q2").Should().Be(File.ReadAllText(BASEDIR + "q2.md"));
            vp.GetVPlan("ef").Should().Be(File.ReadAllText(BASEDIR + "ef.md"));
        }

        [TestMethod]
        public void Test01()
        {
            const string BASEDIR = @"TestData\VPlaner\01\";
            VPlaner vp = new VPlaner();

            vp.UpdatePlan(BASEDIR);
            vp.GetVPlan("6d").Should().Be(File.ReadAllText(BASEDIR + "6d.md"));
            vp.GetVPlan("9g").Should().Be(File.ReadAllText(BASEDIR + "9g.md"));
            vp.GetVPlan("ef").Should().Be(File.ReadAllText(BASEDIR + "ef.md"));
        }
    }
}
